﻿Shader "Custom/Pixelation"
{
	Properties
	{
        _MainTex ("Base (RGB)", 2D) = "white" {}
        _MaskTex ("Mask (RGB)", 2D) = "white" {}
	}
	SubShader
	{
		// No culling or depth
		Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

            uniform sampler2D _MainTex;
            uniform sampler2D _SmallTex;
            uniform sampler2D _MaskTex;

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}

            fixed4 frag (v2f_img i) : COLOR
            {
                #if UNITY_UV_STARTS_AT_TOP
                half2 offset = half2(i.uv.x, 1 - i.uv.y);
                #else
                half2 offset = i.uv;
                #endif

                fixed4 mask = tex2D(_MaskTex, offset);
                if (mask.a != 0) {
                    fixed4 small = tex2D(_SmallTex, offset);
                    return small;
                } else {
                    fixed4 main = tex2D(_MainTex, i.uv); // source
                    return main;
                }
            }
			ENDCG
		}
	}
}