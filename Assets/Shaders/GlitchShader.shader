﻿Shader "Custom/GlitchShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
        _Tess("Tessellation", Range(1,32)) = 4
        _Amplitude("Amplitude", Range(0.01, 1)) = 0.05
        _Frequency("Frequency", Range(0.01, 20)) = 0.5
        _Offset("Frequency Offset", Range(1, 10)) = 1
        _Tine("Color tint", Color) = (1, 1, 1, 1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Standard fullforwardshadows tessellate:tess vertex:vert

		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 4.6
        #define M_PI 3.14159265358979323846
        
		sampler2D _MainTex;
        float _Tess;
        float _Amplitude;
        float _Frequency;
        float _Offset;
        fixed4 _Tint;

        struct appdata {
            float4 vertex : POSITION;
            float3 normal : NORMAL;
            float2 texcoord : TEXCOORD0;
        };

		struct Input {
			float2 uv_MainTex;
		};
       
        
        float4 mod(float4 x, float4 y)
        {
          return x - y * floor(x / y);
        }

        float4 mod289(float4 x)
        {
          return x - floor(x / 289.0) * 289.0;
        }

        float4 permute(float4 x)
        {
          return mod289(((x*34.0)+1.0)*x);
        }

        float4 taylorInvSqrt(float4 r)
        {
          return (float4)1.79284291400159 - r * 0.85373472095314;
        }

        float2 fade(float2 t) {
          return t*t*t*(t*(t*6.0-15.0)+10.0);
        }

        // Classic Perlin noise
        float cnoise(float2 P)
        {
          float4 Pi = floor(P.xyxy) + float4(0.0, 0.0, 1.0, 1.0);
          float4 Pf = frac (P.xyxy) - float4(0.0, 0.0, 1.0, 1.0);
          Pi = mod289(Pi); // To avoid truncation effects in permutation
          float4 ix = Pi.xzxz;
          float4 iy = Pi.yyww;
          float4 fx = Pf.xzxz;
          float4 fy = Pf.yyww;

          float4 i = permute(permute(ix) + iy);

          float4 gx = frac(i / 41.0) * 2.0 - 1.0 ;
          float4 gy = abs(gx) - 0.5 ;
          float4 tx = floor(gx + 0.5);
          gx = gx - tx;

          float2 g00 = float2(gx.x,gy.x);
          float2 g10 = float2(gx.y,gy.y);
          float2 g01 = float2(gx.z,gy.z);
          float2 g11 = float2(gx.w,gy.w);

          float4 norm = taylorInvSqrt(float4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11)));
          g00 *= norm.x;
          g01 *= norm.y;
          g10 *= norm.z;
          g11 *= norm.w;

          float n00 = dot(g00, float2(fx.x, fy.x));
          float n10 = dot(g10, float2(fx.y, fy.y));
          float n01 = dot(g01, float2(fx.z, fy.z));
          float n11 = dot(g11, float2(fx.w, fy.w));

          float2 fade_xy = fade(Pf.xy);
          float2 n_x = lerp(float2(n00, n01), float2(n10, n11), fade_xy.x);
          float n_xy = lerp(n_x.x, n_x.y, fade_xy.y);
          return 2.3 * n_xy;
        }

        // Classic Perlin noise, periodic variant
        float pnoise(float2 P, float2 rep)
        {
          float4 Pi = floor(P.xyxy) + float4(0.0, 0.0, 1.0, 1.0);
          float4 Pf = frac (P.xyxy) - float4(0.0, 0.0, 1.0, 1.0);
          Pi = mod(Pi, rep.xyxy); // To create noise with explicit period
          Pi = mod289(Pi);        // To avoid truncation effects in permutation
          float4 ix = Pi.xzxz;
          float4 iy = Pi.yyww;
          float4 fx = Pf.xzxz;
          float4 fy = Pf.yyww;

          float4 i = permute(permute(ix) + iy);

          float4 gx = frac(i / 41.0) * 2.0 - 1.0 ;
          float4 gy = abs(gx) - 0.5 ;
          float4 tx = floor(gx + 0.5);
          gx = gx - tx;

          float2 g00 = float2(gx.x,gy.x);
          float2 g10 = float2(gx.y,gy.y);
          float2 g01 = float2(gx.z,gy.z);
          float2 g11 = float2(gx.w,gy.w);

          float4 norm = taylorInvSqrt(float4(dot(g00, g00), dot(g01, g01), dot(g10, g10), dot(g11, g11)));
          g00 *= norm.x;
          g01 *= norm.y;
          g10 *= norm.z;
          g11 *= norm.w;

          float n00 = dot(g00, float2(fx.x, fy.x));
          float n10 = dot(g10, float2(fx.y, fy.y));
          float n01 = dot(g01, float2(fx.z, fy.z));
          float n11 = dot(g11, float2(fx.w, fy.w));

          float2 fade_xy = fade(Pf.xy);
          float2 n_x = lerp(float2(n00, n01), float2(n10, n11), fade_xy.x);
          float n_xy = lerp(n_x.x, n_x.y, fade_xy.y);
          return 2.3 * n_xy;
        }

        float4 tess() {
            return _Tess;
        }

        void vert(inout appdata v) {
            float sinX = 10 + sin(_Time.z * v.vertex.y * _Frequency) * _Amplitude;

            float noiseY = cnoise(_Time.z + v.vertex.y * (_Frequency * (sin(_Offset * v.vertex.y)))) * _Amplitude;
            float noiseX = cnoise(_Time.z + v.vertex.x * _Frequency) * _Amplitude;
            float noiseZ = cnoise(_Time.z + v.vertex.z * (_Frequency * (sin(_Offset * v.vertex.z)))) * _Amplitude;

            if(noiseY < noiseX + noiseZ)
                noiseY = 0;
            
            v.vertex.x += noiseY;
            v.vertex.z -= noiseY;

            // Y

            if(v.vertex.y > -0.3 * (sin(_Time.z / 2)) && v.vertex.y < -0.2 * (sin(_Time.z / 2)))
                v.vertex.x += sin(_Time.z / 2) * _Amplitude;

            if(v.vertex.y > -0.4 * (sin(_Time.z / 2)) && v.vertex.y < -0.3 * (sin(_Time.z / 2)))
                v.vertex.x += sin(_Time.z / 2) * _Amplitude / 1.5;

            if(v.vertex.y > 0.2 * (sin(_Time.z / 2)) && v.vertex.y < 0.3 * (sin(_Time.z / 2)))
                v.vertex.x -= sin(_Time.z / 2) * _Amplitude;

            if(v.vertex.y > 0.3 * (sin(_Time.z / 2)) && v.vertex.y < 0.4 * (sin(_Time.z / 2)))
                v.vertex.x -= sin(_Time.z / 2) * _Amplitude / 1.5;

            // X

            //if(v.vertex.x > -0.3 * (sin(_Time.z / 3)) && v.vertex.x < -0.2 * (sin(_Time.z / 3)))
                //v.vertex.x = 0;

            if(v.vertex.x > cnoise(_Time.z) && v.vertex.x < cnoise(_Time.z))
                v.vertex.x += cnoise(_Time.z);
        }

		half _Glossiness;
		half _Metallic;
		fixed4 _Color;

		// Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
		// See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
		// #pragma instancing_options assumeuniformscaling
		UNITY_INSTANCING_BUFFER_START(Props)
			// put more per-instance properties here
		UNITY_INSTANCING_BUFFER_END(Props)

		void surf (Input IN, inout SurfaceOutputStandard o) {
			// Albedo comes from a texture tinted by color
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			// Metallic and smoothness come from slider variables
			o.Metallic = _Metallic;
			o.Smoothness = _Glossiness;
			o.Alpha = c.a;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
