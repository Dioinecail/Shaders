﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Inventory : ScriptableObject {
	public List<Item> items;

	public List<Item> clues;

	public void AddItem(Item item) {
		switch (item.itemType)
		{
			case Item.ItemType.Item:
				items.Add(item);
				break;
			case Item.ItemType.Clue:
				clues.Add(item);
				break;
			default:
				break;
		}
	}

	public void RemoveItem(Item item) {
		switch (item.itemType)
		{
			case Item.ItemType.Item:
				items.Remove(item);
				break;
			case Item.ItemType.Clue:
				clues.Remove(item);
				break;
			default:
				break;
		}
	}

	public void Clear() {
		items.Clear();
		clues.Clear();
	}
}
