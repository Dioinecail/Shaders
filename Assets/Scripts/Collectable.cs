﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : Interactable {
	public Item item;

	public override void Interact()
	{
		if(GameManager.Instance != null)
		{
			GameManager.Instance.inventory.AddItem(item);
			Destroy(gameObject);
		}
	}
}
