﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour 
{
	public Transform target;

	public float speed;
	public Vector2 offset;

	private void FixedUpdate()
	{
		Vector3 targetPosition = target.position + (Vector3)offset;
		targetPosition.z = transform.position.z;

		transform.position = Vector3.Lerp(transform.position, targetPosition, speed * Time.deltaTime);
	}


	private void OnDrawGizmos()
	{
		Gizmos.color = Color.cyan;
		if(target)
		Gizmos.DrawWireSphere(target.position - (Vector3)offset, 0.1f);
	}

}
