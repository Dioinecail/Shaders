﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Item : ScriptableObject {

	public GameObject prefab;

	public ItemType itemType;

	public Collectable GetObject() {
		return prefab.GetComponent<Collectable>();
	}

	public enum ItemType
	{
		Item,
		Clue
	}
}
