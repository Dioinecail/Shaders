﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisualizationSwitcher : MonoBehaviour 
{
	public Material mat;
	public bool on = true;

	private void Start()
	{
		mat = GetComponent<Renderer>().sharedMaterial;
	}

	private void Update()
	{
		if(Input.GetMouseButtonDown(0)) {
			SwitchVisualization();
		}
	}

	void SwitchVisualization() {
		on = !on;

		if(mat.HasProperty("Vector1_3B9D38AE"))
		{
			mat.SetFloat("Vector1_3B9D38AE", on ? 1 : 0);
			Debug.Log("Has property : " + "Enable");
		}
	}
}
