﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractBehaviour : MonoBehaviour {

	public float interactionRadius;
	public bool canInteract;
	public LayerMask interactionMask;

	private Collider[] interactionBuffer = new Collider[1];

	private void Update()
	{
		interactionBuffer = Physics.OverlapSphere(transform.position, interactionRadius, interactionMask);

		canInteract = (interactionBuffer.Length > 0 && interactionBuffer[0].tag == "Interaction");

		if(Input.GetButtonDown("Fire1") && canInteract) {
			Interact(interactionBuffer[0]);
		}
	}

	public void Interact(Collider obj) {
		Debug.Log("Interacted with : " + obj.name);
		Interactable item = obj.GetComponent<Interactable>();

		if(item != null) {
			item.Interact();
		}
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.cyan;
		Gizmos.DrawWireSphere(transform.position, interactionRadius);
	}
}
