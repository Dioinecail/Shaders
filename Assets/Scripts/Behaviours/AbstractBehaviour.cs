﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbstractBehaviour : MonoBehaviour {

	protected Rigidbody rBody;
	protected CollisionBehaviour collisionBehaviour;

	protected virtual void Awake() {
		rBody = GetComponent<Rigidbody>();
		collisionBehaviour = GetComponent<CollisionBehaviour>();
	}
}
