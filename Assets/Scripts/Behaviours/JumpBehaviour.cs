﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpBehaviour : AbstractBehaviour {
	public float jumpForce;

	private void Update()
	{
		if(Input.GetButtonDown("Jump")) {
			Jump();
		}
	}

	private void Jump() {
		if(collisionBehaviour.onGround)
		{
			rBody.velocity = new Vector3(rBody.velocity.x, jumpForce);
		}
	}
}
