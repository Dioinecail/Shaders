﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveBehaviour : AbstractBehaviour {
	public float speed;

	private void Update()
	{
		float x = Input.GetAxis("Horizontal");

		Move(x);
	}

	public void Move(float direction) {
		rBody.velocity = new Vector3(direction * speed, rBody.velocity.y);
	}
}
