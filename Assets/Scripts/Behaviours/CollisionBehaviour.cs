﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionBehaviour : MonoBehaviour {
	public float collisionRadius;
	public bool onGround;
	public Vector3 collisionOffset;

	public LayerMask collisionMask;

	private Collider[] collisionBuffer = new Collider[1];

	private void Update()
	{
		collisionBuffer = Physics.OverlapSphere(transform.position + collisionOffset, collisionRadius, collisionMask);

		onGround = (collisionBuffer.Length > 0 && collisionBuffer[0].tag == "Solid");
	}

	private void OnDrawGizmos()
	{
		Gizmos.color = Color.red;
		Gizmos.DrawWireSphere(transform.position + collisionOffset, collisionRadius);
	}
}
